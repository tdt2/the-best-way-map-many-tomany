package HibernatePoJo;

import HibernatePoJo.PostTagId;

import javax.persistence.*;
import java.util.Objects;

@Entity(name="PostTag")
@Table(name="post_tag")
public class PostTag {
    @EmbeddedId
    private PostTagId ID;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("postId")
    @JoinColumn(name="post_id")
    private Post post;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("tagId")
    @JoinColumn(name="tag_id")
    private Tag tag;
    @Column(name="id")
    private int id;

    public PostTag(Post post, Tag tag) {
        this.post = post;
        this.tag = tag;
        this.ID = new PostTagId(post.getId(),tag.getId());
    }

    public PostTag() {
    }

    public PostTagId getID() {
        return ID;
    }

    public void setID(PostTagId ID) {
        this.ID = ID;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        PostTag that = (PostTag) o;
        return Objects.equals(post, that.post) &&
                Objects.equals(tag, that.tag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(post, tag);
    }

    @Override
    public String toString() {
        return "PostTag{" +
                "ID=" + ID +
                ", post=" + post +
                ", tag=" + tag +
                ", id=" + id +
                '}';
    }
}