package DAO;

import HibernatePoJo.Post;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class PostDAO {
    SessionFactory sessionFactory= new Configuration().configure().buildSessionFactory();
    public static PostDAO getInstance(){
        return new PostDAO();
    }
    public void savePost(Post post){
        Session session= sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(post);
            System.out.println("Save ok !!");
        }
        catch (RuntimeException e){
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            session.flush();
            session.close();
        }
    }
    public void showAll(){
        Session session= sessionFactory.openSession();
        List<Post> list=session.createQuery("from Post ").list();
        list.forEach((Post)-> System.out.println(Post));
    }
}
