package DAO;

import HibernatePoJo.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class TagDAO {
    SessionFactory sessionFactory= new Configuration().configure().buildSessionFactory();
    public static TagDAO getInstance(){
        return new TagDAO();
    }

    public void saveTag(Tag tag){
        Session session= sessionFactory.openSession();
        try{
            session.beginTransaction();
            session.save(tag);
            System.out.println("Save ok !!");
        }
        catch (RuntimeException e){
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            session.flush();
            session.close();
        }
    }

    public void showAll(){
        Session session= sessionFactory.openSession();
        List<Tag> list=session.createQuery("from Tag").list();
        list.forEach((Tag)-> System.out.println(Tag));
        session.close();
    }
}
